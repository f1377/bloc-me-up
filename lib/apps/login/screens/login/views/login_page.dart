import 'package:authentication_repository/authentication_repository.dart';
import 'package:bloc_me_up/apps/login/screens/login/bloc/login_bloc.dart';
import 'package:bloc_me_up/apps/login/screens/login/views/login_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatelessWidget {
  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => const LoginPage());
  }

  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: BlocProvider(
        create: (BuildContext context) {
          return LoginBloc(
            authenticationRepository:
                RepositoryProvider.of<AuthenticationRepository>(context),
          );
        },
        child: const Padding(
          padding: EdgeInsets.all(12.0),
          child: LoginForm(),
        ),
      ),
    );
  }
}
