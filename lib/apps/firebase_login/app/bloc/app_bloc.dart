import 'dart:async';

import 'package:firebase_authentication_repository/firebase_authentication_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:very_good_analysis/very_good_analysis.dart';

part 'app_event.dart';
part 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  final FirebaseAuthenticationRepository _firebaseAuthenticationRepository;
  late final StreamSubscription<User> _userSubscription;

  AppBloc({
    required FirebaseAuthenticationRepository firebaseAuthenticationRepository,
  })  : _firebaseAuthenticationRepository = firebaseAuthenticationRepository,
        super(
          firebaseAuthenticationRepository.currentUser.isNotEmpty
              ? AppState.authenticated(
                  firebaseAuthenticationRepository.currentUser)
              : const AppState.unauthenticated(),
        ) {
    on<AppUserChanged>(_onUserChanged);
    on<AppLogoutRequested>(_onLogoutRequested);
    _userSubscription = _firebaseAuthenticationRepository.user.listen(
      (user) => add(AppUserChanged(user)),
    );
  }

  void _onUserChanged(AppUserChanged event, Emitter<AppState> emit) {
    emit(
      event.user.isNotEmpty
          ? AppState.authenticated(event.user)
          : const AppState.unauthenticated(),
    );
  }

  void _onLogoutRequested(AppLogoutRequested event, Emitter<AppState> emit) {
    unawaited(_firebaseAuthenticationRepository.logOut());
  }

  @override
  Future<void> close() {
    _userSubscription.cancel();
    return super.close();
  }
}
