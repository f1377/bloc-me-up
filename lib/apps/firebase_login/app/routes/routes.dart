import 'package:bloc_me_up/apps/firebase_login/app/screens/login/view/view.dart';
import 'package:flutter/widgets.dart';

import '../app.dart';
import 'package:bloc_me_up/screens/bloc_homepage_screen.dart';

List<Page> onGenerateAppViewPages(AppStatus state, List<Page<dynamic>> pages) {
  switch (state) {
    case AppStatus.authenticated:
      return [BlocHomepageScreen.page()];
    case AppStatus.unauthenticated:
      return [LoginPage.page()];
  }
}
