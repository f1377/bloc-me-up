part of 'timer_bloc.dart';

abstract class TimerEvent extends Equatable {
  const TimerEvent();

  @override
  List<Object> get props => [];
}

// event to inform Bloc that timer should be started
class TimerStarted extends TimerEvent {
  final int duration;

  const TimerStarted({required this.duration});
}

// event to inform Bloc that timer should be paused
class TimerPaused extends TimerEvent {
  const TimerPaused();
}

// event to inform Bloc that timer should be resumed
class TimerResumed extends TimerEvent {
  const TimerResumed();
}

// event to inform Bloc that timer should be reset
class TimerReset extends TimerEvent {
  const TimerReset();
}

// event to inform Bloc that a tick has occurred
// and that it needs to update its state accordingly
class TimerTicked extends TimerEvent {
  final int duration;

  const TimerTicked({required this.duration});

  @override
  List<Object> get props => [duration];
}
