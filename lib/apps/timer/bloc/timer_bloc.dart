import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_me_up/apps/timer/ticker.dart';
import 'package:equatable/equatable.dart';

part 'timer_event.dart';
part 'timer_state.dart';

class TimerBloc extends Bloc<TimerEvent, TimerState> {
  final Ticker _ticker;

  // set to 60 seconds = 1 minute
  static const int _duration = 60;

  StreamSubscription<int>? _tickerSubscription;

  TimerBloc({required Ticker ticker})
      : _ticker = ticker,
        super(const TimerInitial(_duration)) {
    on<TimerStarted>(_onStarted);
    on<TimerTicked>(_onTicked);
    on<TimerPaused>(_onPaused);
    on<TimerResumed>(_onResumed);
    on<TimerReset>(_onReset);
  }

  @override
  // cancel timer first before close
  Future<void> close() {
    _tickerSubscription?.cancel();
    return super.close();
  }

  /* Bloc receives a TimerStarted event
   **
   ** push a TimerRunInProgress state with duration
   **
  */
  void _onStarted(TimerStarted event, Emitter<TimerState> emit) {
    emit(TimerRunInProgress(event.duration));

    // cancel | deallocate prev timer
    _tickerSubscription?.cancel();

    // listen to tick stream on every tick
    // add to TimerTicked event with remaining duration
    _tickerSubscription = _ticker
        .tick(ticks: event.duration)
        .listen((duration) => add(TimerTicked(duration: duration)));
  }

  /* Bloc received a TimerPaused event
   **
   ** if timer is running, pause the timer
   **
  */
  void _onPaused(TimerPaused event, Emitter<TimerState> emit) {
    if (state is TimerRunInProgress) {
      _tickerSubscription?.pause();
      emit(TimerRunPause(state.duration));
    }
  }

  /* Bloc received a TimerResumed event
   **
   ** if timer is paused, resume the timer
   **
  */
  void _onResumed(TimerResumed event, Emitter<TimerState> emit) {
    if (state is TimerRunPause) {
      _tickerSubscription?.resume();
      emit(TimerRunInProgress(state.duration));
    }
  }

  /* Bloc received a TimerReset event
   **
   ** cancel and reset timer
   **
  */
  void _onReset(TimerReset event, Emitter<TimerState> emit) {
    _tickerSubscription?.cancel();
    emit(const TimerInitial(_duration));
  }

  /* Bloc received a TimerTicked event
   **
   ** if duration is greater than 0
   ** push TimerRunInProgress state with new duration
   **
   ** if duration is null push TimerRunComplete state
  */
  void _onTicked(TimerTicked event, Emitter<TimerState> emit) {
    emit(event.duration > 0
        ? TimerRunInProgress(event.duration)
        : const TimerRunComplete());
  }
}
