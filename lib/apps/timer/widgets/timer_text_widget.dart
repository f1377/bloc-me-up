import 'package:bloc_me_up/apps/timer/bloc/timer_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TimerText extends StatelessWidget {
  const TimerText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final int duration =
        context.select((TimerBloc bloc) => bloc.state.duration);

    final String minutes =
        ((duration / 60) % 60).floor().toString().padLeft(2, '0');
    final String seconds = (duration % 60).floor().toString().padLeft(2, '0');

    return Text(
      '$minutes:$seconds',
      style: Theme.of(context).textTheme.headline1,
    );
  }
}
