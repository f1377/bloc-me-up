import 'package:bloc_me_up/apps/timer/bloc/timer_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TimerButtons extends StatelessWidget {
  const TimerButtons({Key? key}) : super(key: key);

  List<Widget> _createFloatActionButtons(
      BuildContext context, TimerState state) {
    List<Widget> buttons = [];

    bool firstButtonSet = false;
    late IconData firstButtonIcon;
    late TimerEvent firstButtonEvent;
    late FloatingActionButton? firstButton;

    bool secondButtonSet = false;
    late IconData secondButtonIcon;
    late TimerEvent secondButtonEvent;
    late FloatingActionButton? secondButton;

    if (state is TimerInitial) {
      firstButtonIcon = Icons.play_arrow;
      firstButtonEvent = TimerStarted(duration: state.duration);
      firstButtonSet = true;
    } else if (state is TimerRunInProgress) {
      firstButtonIcon = Icons.pause;
      firstButtonEvent = const TimerPaused();
      firstButtonSet = true;

      secondButtonIcon = Icons.replay;
      secondButtonEvent = const TimerReset();
      secondButtonSet = true;
    } else if (state is TimerRunPause) {
      firstButtonIcon = Icons.play_arrow;
      firstButtonEvent = const TimerResumed();
      firstButtonSet = true;

      secondButtonIcon = Icons.replay;
      secondButtonEvent = const TimerReset();
      secondButtonSet = true;
    } else if (state is TimerRunComplete) {
      firstButtonIcon = Icons.replay;
      firstButtonEvent = const TimerReset();
      firstButtonSet = true;
    }

    if (firstButtonSet) {
      firstButton = FloatingActionButton(
        heroTag: 'btnTimerFirst',
        child: Icon(firstButtonIcon),
        onPressed: () => context.read<TimerBloc>().add(firstButtonEvent),
      );
      buttons.add(firstButton);
    }
    if (secondButtonSet) {
      secondButton = FloatingActionButton(
        heroTag: 'btnTimerSecond',
        child: Icon(secondButtonIcon),
        onPressed: () => context.read<TimerBloc>().add(secondButtonEvent),
      );
      buttons.add(secondButton);
    }

    return buttons;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TimerBloc, TimerState>(
        buildWhen: (TimerState prev, TimerState state) =>
            prev.runtimeType != state.runtimeType,
        builder: (BuildContext context, TimerState state) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: _createFloatActionButtons(context, state),
          );
        });
  }
}
