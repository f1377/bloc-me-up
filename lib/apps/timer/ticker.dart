class Ticker {
  const Ticker();

  Stream<int> tick({required int ticks}) {
    // returns a Stream which emits every remaining seconds, every second
    return Stream.periodic(const Duration(seconds: 1), (x) => ticks - x - 1)
        .take(ticks);
  }
}
