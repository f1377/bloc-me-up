import 'package:bloc_me_up/apps/timer/bloc/timer_bloc.dart';
import 'package:bloc_me_up/apps/timer/widgets/timer_buttons_widget.dart';
import 'package:bloc_me_up/apps/timer/widgets/timer_background_widget.dart';
import 'package:bloc_me_up/apps/timer/widgets/timer_text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'ticker.dart';

class TimerScreen extends StatelessWidget {
  const TimerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => TimerBloc(ticker: const Ticker()),
      child: const TimerView(),
    );
  }
}

class TimerView extends StatelessWidget {
  const TimerView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const TimerBackgroundWidget(),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 100.0),
              child: Center(
                child: TimerText(),
              ),
            ),
            TimerButtons(),
          ],
        ),
      ],
    );
  }
}
