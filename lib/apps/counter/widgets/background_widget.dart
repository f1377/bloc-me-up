import 'package:flutter/material.dart';

class CounterBackground extends StatelessWidget {
  const CounterBackground({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Colors.green.shade50,
            Colors.green.shade500,
          ],
        ),
      ),
    );
  }
}
