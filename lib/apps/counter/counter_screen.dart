import 'package:bloc_me_up/apps/counter/bloc/counter_cubit.dart';
import 'package:bloc_me_up/apps/counter/widgets/background_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CounterScreen extends StatelessWidget {
  const CounterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => CounterCubit(),
      child: const CounterView(),
    );
  }
}

class CounterView extends StatelessWidget {
  const CounterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).textTheme;

    return Center(
      child: BlocBuilder<CounterCubit, int>(
        builder: (BuildContext context, int state) {
          return Stack(
            children: [
              const CounterBackground(),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 100.0),
                    child: Text(
                      '$state',
                      style: textTheme.headline2,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      FloatingActionButton(
                        heroTag: 'btnCounterRemove',
                        child: const Icon(Icons.remove),
                        onPressed: () {
                          if (state <= 0) {
                            return;
                          }

                          return context.read<CounterCubit>().decrement();
                        },
                      ),
                      FloatingActionButton(
                        heroTag: 'btnCounterAdd',
                        child: const Icon(Icons.add),
                        onPressed: () =>
                            context.read<CounterCubit>().increment(),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          );
        },
      ),
    );
  }
}

class Background extends StatelessWidget {
  const Background({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Colors.blue.shade50,
            Colors.blue.shade500,
          ],
        ),
      ),
    );
  }
}
