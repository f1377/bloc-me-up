import 'package:bloc_me_up/apps/counter/counter_screen.dart';
import 'package:bloc_me_up/apps/infinite_list/infinite_list_screen.dart';
import 'package:bloc_me_up/apps/login/authenticat%C3%ADon/bloc/authentication_bloc.dart';
import 'package:bloc_me_up/apps/timer/timer_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocHomepageScreen extends StatefulWidget {
  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => const BlocHomepageScreen());
  }

  static Page page() => const MaterialPage<void>(child: BlocHomepageScreen());

  const BlocHomepageScreen({Key? key}) : super(key: key);

  @override
  State<BlocHomepageScreen> createState() => _BlocHomepageScreenState();
}

class _BlocHomepageScreenState extends State<BlocHomepageScreen> {
  int _navIndex = 0;

  static const List<Widget> _navOptions = [
    CounterScreen(),
    TimerScreen(),
    InfiniteScreen(),
  ];

  static const List<BottomNavigationBarItem> _navItems = [
    BottomNavigationBarItem(
      icon: Icon(Icons.confirmation_number),
      label: 'Counter',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.timer),
      label: 'Timer',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.list_alt_outlined),
      label: 'Infinite List',
    ),
  ];

  void _onNavTap(int index) {
    setState(() {
      _navIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text('Bloc Me Up')),
        actions: [
          IconButton(
              onPressed: () {
                context
                    .read<AuthenticationBloc>()
                    .add(AuthenticationLogoutRequested());
              },
              icon: const Icon(Icons.logout)),
        ],
      ),
      body: Center(
        child: _navOptions.elementAt(_navIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _navIndex,
        items: _navItems,
        onTap: _onNavTap,
      ),
    );
  }
}
