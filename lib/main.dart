import 'package:authentication_repository/authentication_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:bloc_me_up/apps/login/authenticat%C3%ADon/bloc/authentication_bloc.dart';
import 'package:bloc_me_up/apps/login/screens/login/views/login_page.dart';
import 'package:bloc_me_up/apps/login/screens/splash/splash_screen.dart';
import 'package:bloc_me_up/bloc_apps_observer.dart';
import 'package:bloc_me_up/screens/bloc_homepage_screen.dart';
import 'package:firebase_authentication_repository/firebase_authentication_repository.dart';
import 'package:flow_builder/flow_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_repository/user_repository.dart';
import 'package:firebase_core/firebase_core.dart';

import 'apps/firebase_login/app/app.dart';
import 'apps/firebase_login/app/routes/routes.dart';
import 'apps/firebase_login/theme/theme.dart';
import 'firebase_options.dart';

Future<void> main() async {
  BlocOverrides.runZoned(
    () async {
      WidgetsFlutterBinding.ensureInitialized();
      await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform,
      );
      final firebaseAuthenticationRepository =
          FirebaseAuthenticationRepository();
      await firebaseAuthenticationRepository.user.first;

      runApp(BlocApp(
        authenticationRepository: AuthenticationRepository(),
        userRepository: UserRepository(),
        firebaseAuthenticationRepository: FirebaseAuthenticationRepository(),
      ));
    },
    blocObserver: BlocAppsObserver(),
  );
}

class BlocApp extends StatelessWidget {
  final AuthenticationRepository authenticationRepository;
  final UserRepository userRepository;
  final FirebaseAuthenticationRepository firebaseAuthenticationRepository;

  bool useFirebaseLogin = true;

  BlocApp(
      {Key? key,
      required this.authenticationRepository,
      required this.userRepository,
      required this.firebaseAuthenticationRepository})
      : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    if (useFirebaseLogin) {
      return RepositoryProvider.value(
        value: firebaseAuthenticationRepository,
        child: BlocProvider(
          create: (_) => AppBloc(
            firebaseAuthenticationRepository: firebaseAuthenticationRepository,
          ),
          child: const AppFirebaseView(),
        ),
      );
    } else {
      return RepositoryProvider.value(
        value: authenticationRepository,
        child: BlocProvider(
          create: (_) => AuthenticationBloc(
            authenticationRepository: authenticationRepository,
            userRepository: userRepository,
          ),
          child: const AppView(),
        ),
      );
    }
  }
}

class AppFirebaseView extends StatelessWidget {
  const AppFirebaseView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: theme,
      home: FlowBuilder<AppStatus>(
        state: context.select((AppBloc bloc) => bloc.state.status),
        onGeneratePages: onGenerateAppViewPages,
      ),
    );
  }
}

class AppView extends StatefulWidget {
  const AppView({Key? key}) : super(key: key);

  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  NavigatorState get _navigator => _navigatorKey.currentState!;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      navigatorKey: _navigatorKey,
      builder: (BuildContext context, child) {
        return BlocListener<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            switch (state.status) {
              case AuthenticationStatus.authenticated:
                _navigator.pushAndRemoveUntil<void>(
                  BlocHomepageScreen.route(),
                  (route) => false,
                );
                break;
              case AuthenticationStatus.unauthenticated:
                _navigator.pushAndRemoveUntil(
                  LoginPage.route(),
                  (route) => false,
                );
                break;
              default:
                break;
            }
          },
          child: child,
        );
      },
      onGenerateRoute: (_) => SplashScreen.route(),
    );
  }
}
