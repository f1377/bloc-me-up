# Bloc Me Up [2022]

Building small Apps by using BLOC dependencies [Following Tutorials].

## Sources
 - [Counter](https://bloclibrary.dev/#/fluttercountertutorial)
 - [Timer](https://bloclibrary.dev/#/fluttertimertutorial)
 - [Infinite List](https://bloclibrary.dev/#/flutterinfinitelisttutorial)
 - [Login](https://bloclibrary.dev/#/flutterlogintutorial)
 - [Weather](https://bloclibrary.dev/#/flutterweathertutorial)
 - [Todos](https://bloclibrary.dev/#/fluttertodostutorial)
 - [Firebase Login](https://bloclibrary.dev/#/flutterfirebaselogintutorial)

## Dependencies
 - [bloc](https://pub.dev/packages/bloc)
 - [flutter_bloc](https://pub.dev/packages/flutter_bloc)
 - [equatable](https://pub.dev/packages/equatable)

## App Overview

### Counter Screen
![Counter Screen](/images/readme/counter_screen.png "Counter Screen")

### Timer Screen
![Timer Screen](/images/readme/timer_screen.png "Timer Screen")
